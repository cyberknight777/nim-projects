let n = ("Banana", 2, 'c')
echo n

# one can also name each field in a tuple to distinguish them.

var o = (name: "Ban", weight: 3, rating: 'b')

o[1] = 7
o.name = "Apple"
echo o
