proc isdivisibleby3(x: int): bool =
  return x mod 3 == 0

proc filtermultiplesof3(a: seq[int]): seq[int] =
  # result = @[]
  for i in a:
    if i.isdivisibleby3():
      result.add(i)

let
  g = @[2, 6, 5, 7, 9, 0, 5, 3]
  h = @[5, 4, 3, 2, 1]
  i = @[626, 45390, 3219, 4210, 4126]

echo filtermultiplesof3(g)
echo h.filtermultiplesof3()
echo filtermultiplesof3 i
