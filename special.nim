echo "some\nim\tips"

# \n = newline
# \t = tab
# \\ backslash since \ is used as an escape character

echo "some\\nim\\tips" # this can work to print the string as it is

echo r"some\nim\tips" # r "..." to print raw strings
