let j = ['a', 'b', 'c', 'd', 'e']

# Like other langs, nim also counts first element as index zero so second element is index 1 and etc.

# Indexing from the back gotta use ^ prefix like ^1 is the last element.

echo j[1]
echo j[^1]

# Slicing is basically like for loop start .. stop

echo j[0..3]
echo j[0..<3]
