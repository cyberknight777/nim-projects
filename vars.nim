var
  a: int,
  b = 6,
  c: int = 9,
  d = 'Hi'

var f = 69

f = 4
f = 2
f = 0

# These are comments

const j = 3
j = -3 # wrong

var h = -5
const i = h + 4 # wrong as value of i cannot be known at compile time

let l = 9
l = -9 # wrong

var k = -6
let m = k + 7 # this works as let's value can be set at anytime but once it is set it cannot be changed
