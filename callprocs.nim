proc findmax(x: int, y: int): int =
  if x > y:
    return x
  else:
    return y
  # this is inside of the procedure
# this is outside of the procedure

let
  a = findMax(987, 789)
  b = findMax(123, 321)
  c = findMax(a, b)

echo a
echo b
echo c

