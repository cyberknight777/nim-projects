# countup() is to iterate through a range of numbers with a step size different than 1.

for n in countup(0, 16, 4):  
  echo n

# To iterate through a range of numbers where start is larger than finish, we use countdown().
# Even if we're counting down, the step size must be positive.

echo ""

for n in countdown(4, 0):       
  echo n

echo ""

for n in countdown(-3, -9, 2):  
  echo n
