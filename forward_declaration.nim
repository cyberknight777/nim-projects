#echo 5.plus(10) # error

#proc plus(x, y: int): int =
#  return x + y

# The above would throw an error as plus isn't defined yet.

proc plus(x, y: int): int

echo 5.plus(10)

proc plus(x, y: int): int =
  return x + y

# The above is a proper forward declaration.
