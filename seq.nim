var
  g = @['x', 'y']
  h = @['1', '2', '3']

g.add('z')  
echo g

h.add(g)    
echo h

# Trying to add different types would cause an error.

echo "\nSequences can vary in length hence to get its length one can use len."

echo ""

var i = @[6, 9, 4, 2]
echo i.len

i.add(0)
echo i.len

