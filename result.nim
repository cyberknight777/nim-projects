proc findbiggest(a: seq[int]): int =
  for number in a:
    if number > result:
      result = number
  # the end of proc

proc findsmallest(b: seq[int]): int =
  for n in b:
    if n < result:
      result = n
  # the end of proc

let d = @[3, -5, 11, 33, 7, -15]
echo findbiggest(d)
echo ""
echo findsmallest(d)

echo ""

proc keepodds(a: seq[int]): seq[int] =
  # result = @[]
  for number in a:
    if number mod 2 == 1:
       result.add(number)

let f = @[1, 6, 4, 43, 57, 34, 98]
echo keepodds(f)
