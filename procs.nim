#proc changeArgument(argument: int) =
#  argument += 5

#var ourVariable = 10
#changeArgument(ourVariable)

# The example above wont work because we are not allowed to change given parameters.

proc changeArgument(argument: var int) =
  echo "Adding 5 to the argument provided..."
  argument += 5

var ourVariable = 10
changeArgument(ourVariable)
echo ourVariable
changeArgument(ourVariable)
echo ourVariable

# Essentially we declared argument as a variable which made it being inter-changeable.

echo ""

var x = 100

proc echoX() =
  echo x  
  x += 1  

echoX()
echoX()

# Its a good practice to pass things as arguments but its possible to use names declared outside the procedure such as above.

